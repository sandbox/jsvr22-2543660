<?php

/**
 * @file
 * Panzoom theme functions.
 */

/**
 * Returns HTML for an Panzoom image field formatter.
 *
 * @param $variables
 *   An associative array containing:
 *   - item: An array of image data.
 *   - image_style: An optional image style.
 *   - path: An array containing the link 'path' and link 'options'.
 *
 * @ingroup themeable
 */
function theme_panzoom_image_formatter($variables) {
  $item = $variables['item'];
  $entity_type = $variables['entity_type'];
  $entity = $variables['entity'];
  $field = $variables['field'];
  //$settings = $variables['display_settings'];

  $id='panzoom_'.$variables['delta'];
  $image = array(
    'path' => $item['uri'],
    'alt' => isset($item['alt']) ? $item['alt'] : '',
    'title' => isset($item['title']) ? $item['title'] : '',
    'attributes' => array('class' => 'panzoom', 'id' => $id),
  );

    $path = file_create_url($image['path']);

  return theme('panzoom_imagefield', array('image' => $image, 'path' => $path,));
}

/**
 * Returns HTML for an image using a specific Panzoom image style.
 *
 * @param $variables
 *   An associative array containing:
 *   - image: image item as array.
 *   - path: The path of the image that should be displayed in the Panzoom.
 *   - title: The title text that will be used as a caption in the Panzoom.

 *
 * @ingroup themeable
 */
function theme_panzoom_imagefield($variables) {
  
     $image = theme('image', $variables['image']);
     
     $output ='<div class="panzoom-parent">';
     $output .=$image;
     $output.='<div class="buttons">';
     $output .='<div class="btn_icon">';
    $output .='<button class="zoom-in">Zoom In</button>';
    $output .='<input type="range" class="zoom-range">';
    $output .='<button class="zoom-out">Zoom Out</button>';
    $output .='<button class="reset">Reset</button>';
    $output .='</div></div>';
    $output .='</div>';
        return $output;

        


}
